# DotFiles

## What are dotfiles

Dotfiles are the customization files (their filenames usually begin with a period) that are used to personalize your Linux or other Unix-based systems. This repository contains my personal dotfiles.



## Dependencies
* Window Manager `i3`
* Terminal `alacritty` , 'cool-retro-term'
* Shell `fish`
* Bottom-Tray `polybar`
* Wallpaper manager `Nitrogen`
* Compositer `Picom`
* File-Explorer `pcmanfm`
* Editor `doom-emacs`
* Alt Editor `VSCode`
* browser `firefox`
* Audio-Utilities `alsa-utils`
* Network-manager `nmcli`

> Arch-Linux Flavour : `Garuda-Linux`

## Screenshots

### Theme : Mono-poly
![home](assets/home2.png)


---
![apps](assets/widgets.png)
<br>
----



----

<br>

### Theme : Retro-Gruv


![home](assets/home3.png)

----------------------------------------------------------
![apps](assets/vim_home3.png)

<br>

## Custom scripts
Custom script to parse git initialized folder and check for unstaged commits 
![file](assets/synker.png)

## Installation 
> :warning: backup is created of your old config files

```bash
git clone --recursive https://gitlab.com/holychicken99/dotfiles
cd dotfiles
chmod +x install.sh
./install
```
> you will be asked to choose the wallpaper you want nitrogen to apply


# How To Manage Your Own Dotfiles
There are a hundred ways to manage your dotfiles. Personally, I use the *git bare repository method* for managing my dotfiles. Here is an article about this method of managing your dotfiles: [[https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/][https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/]]

Other articles discussing dotfile management are listed below:
- [Managing dotfiles with style with rcm Ronnie Nissan](https://distrotube.com/guest-articles/managing-dotfiles-with-rcm.html)

- [Interactive dotfile management with dotbare Kevin Zhuang](https://distrotube.com/guest-articles/interactive-dotfile-management-dotbare.html)

## Credits ##
[polybar-themes](https://github.com/adi1090x/polybar-themes)
