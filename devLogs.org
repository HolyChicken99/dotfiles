#+title: Dev Logs
#+Author: Holychicken

- SSDM the login managers starts the X.org server , looks at the files here and starts the session
-- .desktop file in /usr/share/xsession
- Other option is to manually start the xsession using xinit in the tty
- Login manager/Display manager is the application that starts after the boot process ends instead of tty ?
- To enable graphical login, enable the appropriate systemd service. For example, for SDDM, enable sddm.service.
-- The After boot the daemons are started
