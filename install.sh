#!/bin/bash
#    ___       ___
#   /\__\     /\  \
#  /:/__/_   /::\  \  HolyChicken99
# /::\/\__\ /:/\:\__\ https://gitlab.com/HolyChicken99/dotfiles
# \/\::/  / \:\ \/__/
#   /:/  /   \:\__\
#   \/__/     \/__/

# NAME : HC dotfiles.
# DESC : Dotfiles for my Arch-Linux setup.
# WARNING : RUN THIS SCRIPT AT YOUR OWN RISK.
# DEPENDENCIES : i3-gaps, polybar , git , alacritty.

# check if the user is sudo
# check if i3 , ranger , alacritty , polybar , git  are installed
# copy the files to specific locations

if [ "$(id -u)" != 0 ]; then
    echo "##################################################################"
    echo "This script MUST  be run as root user since it makes changes"
    echo "to the \$HOME directory of the \$USER executing this script."
    echo "We also need permission to check if some Pacman Packages are "
    echo " installed or not "
    "##################################################################"
    exit 1
    
fi

pacman_package_confirmation(){
 dialog --colors --title "\Z7\Do you want to install""$1" --yes-label "Begin Installation" --no-label "Exit" --yesno "\Z4Install" 8 60 || { clear; exit 1; }
}


error() {
    clear
    printf "ERROR:\\n%s\\n" "$1" >&2
    exit 1
}


echo " Checking if necessary Packages are installed or not " | lolcat
package1 =i3-gaps
package2 =polybar
package3 =git
package4 =alacritty

if pacman -Qs $package1 >/dev/null; then
    echo " $package1 is installed"
else
    error "$package1 is not installed"
    exit 1
fi

if pacman -Qs $package2 >/dev/null; then
    echo " $package2 is installed"
else
    error "$package2 is not installed"
    exit 1
fi

if pacman -Qs $package3 >/dev/null; then
    echo " $package3 is installed"
else
    error "$package3 is not installed"
    exit 1
fi

if pacman -Qs $package4 >/dev/null; then
    echo " $package4 is installed"
else
    error "$package4 is not installed"
    exit 1
fi


dotFiles_Installation_confirmation (){
    dialog --colors --title "\Z7\ZbInstallation will begin Now " --yes-label "Begin Installation" --no-label "Exit" --yesno "\Z4Shall we begin Customizing your i3?" 8 60 || { clear; exit 1; }
}

dotFiles_Installation_confirmation || error "User opted out ..... \\n  exiting..."

echo "################################################################"
echo " Copying Config files..."
echo "################################################################"



# TODO: 
# copy to i3
# copy to alacritty
# copy to picom

# i3

[ ! -d ~/.config/i3 ] && mkdir ~/.config/i3 
cd ~/.config/i3 && mkdir i3-backup && cp -r * ./i3-backup 
cp ./i3/config ~/.config/i3/

# picom
[! -d] ~/.config/picom && mkdir ~/.config/picom
cd ~/.config/picom && mkdir picom-backup && cp -r * ./picom-backup
cp ./picom/picom.conf ~/.config/picom

#alacritty
[! -d] ~/.config/alacritty && mkdir ~/.config/alacritty
cd ~/.config/alacritty && mkdir alacritty-backup && cp -r * ./alacritty-backup
cp ./alacritty/alacritty.yml ~/.config/alacritty/

# nitrogen
$ nitrogen ./assets/wallpaper

# polybar-Themes
[! -d] ./polybar-themes && error "could not find the polybar-themes .... "
cd ./polybar-themes && chmod +x setup.sh 
echo "1" | ./setup.sh


#FIXME: add pacman installation for not already installed packages
