#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void error_popup(char *errname) {
  char preface[100] = "kdialog --error \" ";
  strcat(preface, errname);
  strcat(preface, "\"");
  printf("%s", preface);
  system(preface);
}
int main() {

  FILE *ptr = fopen("/sys/class/power_supply/BAT0/capacity", "r");
  if (ptr == NULL) {
    printf("file not opened ");
    return 0;
  }
  int batt;
  fscanf(ptr, "%d", &batt);
  if (batt == 10) {
    error_popup("Battery Level lower than 10%");
    return 0;
  }
  return 0;
}
