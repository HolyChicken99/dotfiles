#!/usr/bin/env bash

set -xe

mkdir src/
mkdir include
touch premake5.lua

boilerplate=' workspace "HelloWorld" \n
   configurations { "Debug", "Release" } \n

project "HelloWorld" \n
   kind "ConsoleApp" \n
   language "C" \n
   targetdir "bin/%{cfg.buildcfg}" \n

   files { "**.h", "**.c" } \n

   filter "configurations:Debug" \n
      defines { "DEBUG" } \n
      symbols "On" \n

   filter "configurations:Release" \n
      defines { "NDEBUG" } \n
      optimize "On" \n
'
echo -e $boilerplate > premake5.lua
