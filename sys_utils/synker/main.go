package main

import (
	// "fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"strings"
	// "time"
)

const (
	needPush = "Changes not staged for commit"
	noPush   = "nothing to commit"
)

func get_dirs() []string {

	userDir := "/home/lpha/Documents/projects"
	files, err := ioutil.ReadDir(userDir)
	if err != nil {

		log.Fatal(err)
	}
	var allDirs []string

	for _, f := range files {
		dir := userDir + "/" + f.Name()

		cmd := exec.Command("git", "status")
		cmd.Dir = dir
		out, _ := cmd.Output()
		output := string(out[:])
		if strings.Contains(output, "Untracked") || strings.Contains(output, "Changes not staged for commit") {
			allDirs = append(allDirs, dir)
		}
	}
	return allDirs

}

func main() {
	var pol = get_dirs()
	var newlined = ""
	for _, f := range pol {
		newlined = newlined + f + "\n"
	}

	if len(pol) != 0 {
		exec.Command("notify-send", "Not Synced with Upstream ⚠️ \n ", newlined, "-h", "string:bgcolor:#403f3d", "-h", "string:fgcolor:#addedc", "-h", "string:frcolor:#ffffff").Output()
	}

}
