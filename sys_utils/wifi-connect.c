#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// WIFI-PROMPT: start at log in
// search for `ANI-5GB` in the list of passwords
// if yes then connect
// TODO: use kde-wallet to access the wifi password
// add prompt to say succesfulle connected

void error_popup(char *errname) {
  char preface[100] = "kdialog --error \" ";
  strcat(preface, errname);
  strcat(preface, "\"");
  printf("%s", preface);
  system(preface);
}

int main(void) {

  int status = system("nmcli -p dev wifi list | grep \"ANI\" > /dev/null");
  if (status == 0) {
    if (system("nmcli dev wifi connect ANI-5GB password \"@4452985t\" ") == 0) {
      ;
    } else {
      error_popup("error with password piping ");
    }
    return 0;
  }
  error_popup("error with nmcli grepping ");
  return 0;
}
